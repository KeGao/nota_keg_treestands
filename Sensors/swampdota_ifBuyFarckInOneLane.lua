local sensorInfo = {
	name = "ifBuyFarck",
	desc = "returns a boolean whether buying a farck or not(also responsible for deleting the invalid farcks)",
	author = "KeGao",
	date = "2022-06-07",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end


--this is kind of static variable?
local lastUpgradeTimes = global.upgradeTimes
function IfBuyFarck(currentFarcksInTheLane, reservationSystemMemory, lastUpgradeTimes)
	local missionInformation = core.MissionInfo()
	--needs to be modified
	local metalThreshold = missionInformation.metalThreshold
	local prices = missionInformation.prices
	--not correct
	local farckPrice = prices["farck"]
	local teamID = Spring.GetMyTeamID()
	
	local currentLevel, storage, _, _, _, _, _, _ = Spring.GetTeamResources (teamID, "metal")

	local teamUnits = Spring.GetTeamUnits(teamID)

	--[[
	local farcks = {}
	for index, unitID in ipairs(teamUnits) do
		local unitDefID = Spring.GetUnitDefID(unitID)
		if UnitDefs[unitDefID].name ~= "armfark" then
			table.insert(farcks, unitID)
		end
	end
	]]

	
	for index, unitID in ipairs(currentFarcksInTheLane) do
		if Spring.ValidUnitID(unitID) == false then
			currentFarcksInTheLane[index] = nil
			reservationSystemMemory.farcks[unitID] = nil
		end
	end
	

	if global.upgradeTimes > lastUpgradeTimes and currentLevel >= farckPrice and #currentFarcksInTheLane <= 2 then
		lastUpgradeTimes = global.upgradeTimes
		return true
	end


end

return function(currentFarcksInTheLane, reservationSystemMemory, lastUpgradeTimes)	
	return IfBuyFarck(currentFarcksInTheLane, reservationSystemMemory, lastUpgradeTimes)
end