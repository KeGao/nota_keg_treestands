local sensorInfo = {
	name = "onePathGeneration",
	desc = "find the one path from the transporter to the unit to be rescued",
	author = "KeGao",
	date = "2021-07-17",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end


function onePathGeneration(farckID, lane, metalID)
	
	local points = lane.points

	local unitX, unitY, unitZ = Spring.GetUnitPosition(farckID)
	local nearestPointIndexForFarck = nil
	local nearestDistanceForFarck = math.huge
	for index, point in pairs(points) do
		if math.sqrt((unitX - point.x) * (unitX - point.x) + (unitZ - point.z) * (unitZ - point.z)) < nearestDistanceForFarck then
			nearestPointIndexForFarck = index
		end
	end

	local metalX, metalY, metalZ = Spring.GetUnitPosition(metalID)
	local nearestPointIndexForMetal = nil
	local nearestDistanceForMetal = math.huge
	for index, point in pairs(points) do
		if math.sqrt((metalX - point.x) * (metalX - point.x) + (metalZ - point.z) * (metalZ - point.z)) < nearestDistanceForFarck then
			nearestPointIndexForMetal = index
		end
	end

	
	local one = {}

	if nearestPointIndexForFarck > nearestPointIndexForMetal then
		for i = nearestPointIndexForMetal, nearestPointIndexForFarck do
			table.insert(one, {points[i].x, points[i].z})
		end
	else
		for i = nearestPointIndexForFarck, nearestPointIndexForMetal, -1 do
			table.insert(one, {points[i].x, points[i].z})
		end
	end

	
	if one == {} then
		Spring.Echo("here01")
	end
		
	return one
end

-- @description return commander position
return function(farckID, lane, battleFrontIndex, metalID)
		
	local one = onePathGeneration(farckID, lane, battleFrontIndex, metalID)
	local count = 0
	
	for i = 1, #one do
		count = count + 1
		if (Script.LuaUI('exampleDebug_update')) then
				Script.LuaUI.exampleDebug_update(
					count, --count, --i * k, -- key
					{	-- data
						startPos = Vec3(one[i][1] * 50 - 30, 500 ,one[i][2] * 50 - 25), 
						endPos = Vec3(one[i][1] * 50 - 20, 500 ,one[i][2] * 50 - 25),
						r = 255/255,
						g = 255/255,
						b = 255/255
					}
				)
		end
	end
	
	return one
end